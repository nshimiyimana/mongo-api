//const passwordComplexity = require('joi-password-complexity');
/*

we don't need thes becouse we have been loaded in user model
const config=require('config');
const Jwt=require('jsonwebtoken');
*/
const asyncMiddleware=require('../middleware/async');
const auth=require('../middleware/auth');
 const config=require('config');
const bcrypt=require('bcrypt');
const _=require('lodash');
const {User,validate}=require('../models/user');
const mongoose=require('mongoose');
const express=require('express');
const { route } = require('./customers');
const router=express.Router();

//getting current user and here we need auth middle ware this is for authorisation


router.get('/me',auth,async (req,res)=>{//req.user._id as we have req.user in implementation of mware funct
const user=await User.findById(req.user._id).select('-password');
res.send(user);
});



router.post('/',async (req,res)=>{
    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

//make sure user not exist in db
let user= User.findOne({email:req.body.email});
if(user) return res.status(400).send('sorry user already registered.')

/*
 users=new User({
    name:req.body.name,
    email:req.body.email,
    password:req.body.password
});
use lodash below instead of this


*/
/*
const complexityOptions = {
    min: 10,
    max: 30,
    lowerCase: 1,
    upperCase: 1,
    numeric: 1,
    symbol: 1,
    requirementCount: 2,
  }
  */



  /* use JWT Debugger to verify user have logged in eg if is admin paste json web token you see after login  */
   
  
  


user=new User(_.pick(req.body,['name','email','password']));


const salt=await bcrypt.genSalt(10);
user.password=await bcrypt.hash(user.password,salt);
//passwordComplexity(complexityOptions).validate(user.password);











await user.save();
/*
res.send(users);  or
res.send({
    _id:user._id,

    name:user.name,
    email:user.email,
    password:user.password
})
use lodash below instead of this
*/




const token=user.generateAuthToken();

res.header('x-auth-token',token).send(_.pick(user,['_id','name','email','password']));

});
















router.get('/',async(req,res)=>{ //you can throw anexception or use promece then and catch to handle error in db eg user.find() .then().catch()
   /*
   // first way to hundle error

 User.find().sort('name').select({_id:0})
   .then((result)=>res.send(result))
   .catch(()=>res.status(500).send('some thing failed in server'));
   
    */

  // second way
    //try{ // with this way we have to go on each route hundler to modify err exption message but express has  error moddle ware easy way to hundle(or to log exception) in only single placewe do it next

let users=User.find().sort('name').select({_id:0}); //here we abolish await to throw error


res.send(users);
  
    //catch(ex){
//next(ex)

       // res.status(500).send('some thing failed');//putted in central in separete model in middleware folder
    //}

})  ;



module.exports =router;
//api/users


