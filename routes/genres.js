const validateObjectid=require('../middleware/validateObjectid'); //is this we gonna to place in /api/genres/:id route handler
const admin = require('../middleware/admin');
const auth=require('../middleware/auth')

const {Genre,validate}=require('../models/genre');

const mongoose=require('mongoose');
const express=require('express');


const router=express.Router();


router.get('/',async (req,res)=>{
    const genres=await Genre.find().sort('name');
    res.send(genres);
});
router.get('/:id',validateObjectid, async (req,res)=>{ //here we insert validateobject id middleware if not this in test failes becouse received status code and expected status code are not be the same/not meet
    //below is how to validate objectids
   // if(!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).send('invalid id') //to check if id not found store status =404 becouse in err it store 500 mean internal server err if you expect 404 it receive 500 becouse it setten in middleware-error  when err in requ&resp pipelines
    const genre=await Genre.findById(req.params.id);
    if(!genre) return res.status(404).send('genre with the given id not found');
    res.send(genre);
});

router.post('/',auth,async (req,res)=>{
    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

const genre=new Genre({
    name:req.body.name
});
const result=await genre.save();
res.send(result);

});


router.put('/:id',[auth,validateObjectid], async (req,res)=>{
const {error} =validate(req.body);
if(error) return res.status(400).send(error.details[0].message);


const genre=await Genre.findByIdAndUpdate(req.params.id,
    {name:req.body.name},
    {new:true});

if(!genre) return res.status(404).send('genre not found with the given id')

res.send(genre);
});

router.delete('/:id',[auth,admin,validateObjectid],async (req,res)=>{ //this 2 middleware functions will be excuted in sequence
 const genre = await Genre.findByIdAndRemove(req.params.id);
if(!genre) return res.status(404).send('genre with given id not found')
res.send(genre);
})



module.exports = router;