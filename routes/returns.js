const Joi=require('joi');
const validate=require('../middleware/validate');
const {Rental}=require('../models/rental');
const {Movie}=require('../models/movies');

const auth=require('../middleware/auth');
const express=require('express');



const router=express.Router();
//we gonna create middle ware funct to be dianamical to both validation either mov,rental,...not on return only
/*

const validate=(validator)=>{
  return (req,res,next)=>{ //when you use this you are able to put this in separate file then use this validate middle ware to every file you want becouse can be dynamically change reference or argument passed in calling validate() in route handler
    const {error}=validator(req.body);
    if(error) return res.status(400).send(error.details[0].message)
    
    next();
}
}*/


router.post('/',[auth,validate(validateReturn)], async (req,res)=>{
//here we want to write a simplest code to make failed test pass below becouse we don't send customerid in body of req we check by custid test to be passed
/*if(!req.body.customerId) return res.status(400).send('customerId is not provided')//if no its value sended
if(!req.body.movieId) return res.status(400).send('movieid is not provided');

//we replace thise 2lines with joi validation
*/



//eg with mong static method
const rental=await Rental.lookup(req.body.customerId,req.body.movieId);//here we are able to use req.body.customer&mov becouse we request in route handler //static meth we have defined in rental model allow to findOne with certain properties of object of class


if(!rental) return res.status(404).send('Rental not found');
if(rental.dateReturned) return res.status(400).send('return already processed');


if(rental.dateReturned) return res.status(400).send('return already processed');


await rental.return();

await rental.save();

//here we make increase stock test to be pass we require update to increase stock
 await Movie.update({_id:rental.movie._id},{$inc:{numberInStock:1}
});





return res.status(200).send(rental);//we pass rental to test rental in body of respons

// here we write code to make test past ex in test we have 401 and here we must have status of 401 in response(simplest code as tdd)



//next we refactor with joi vali replace with normal vali to input customerId&movieId




});


function validateReturn(req) {
  const schema={
      customerId:Joi.objectId().required(),//when you not lod joi-objectId in index.js objectId() not work
      movieId:Joi.objectId().required()

  };
  return Joi.validate(req,schema);
  
}

module.exports =router;