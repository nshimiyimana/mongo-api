const {Movie,validate}=require('../models/movies');
const {Genre}= require('../models/genre');
const mongoose=require('mongoose');
const express=require('express');

const router=express.Router();


router.get('/', async (req,res)=>{
    const movies= await Movie.find().sort('name');

    if(!movies){ return res.send('please you need to save any data in  database..')}
    else if(movies == 0){ res.send('please no data saved database..')}
    else{
    res.send("<html><head><title>movies</title><style>h1{background-color:green;color:white;}</style></head><body><h1 id='replace'>hello this is lovely movies in 200</h1><button onclick='replace();'>continue</button><script> function replace(){ document.getElementById('replace').innerHTML='OUR MOVIES IS NOT FOUND IN STOCK'}</script></body></html>"+movies);
    }
});20
router.post('/',async (req,res)=>{
const {error}=validate(req.body);
if(error) return res.status(400).send(error.details[0].message);
const genre=await Genre.findById(req.body.genreId);
if(!genre) return res.status(400).send('invalid genre')


const movie=new Movie({
title:req.body.title,
genre:{
    _id:genre._id,
    name:genre.name
},
numberInStock:req.body.numberInStock,
dailyRentalRate:req.body.dailyRentalRate



});

const result= await movie.save()
res.send(result);
console.log(result)




})


router.get("/:movieId",async (req,res)=>{

const movie=await Movie.findById(req.params.movieId);
if(!movie) return res.status(400).send('movie with given id not found')
res.send(movie);

});


router.put("/:movieId",async (req,res)=>{

const {error}=validate(req.body);
if(error) return res.status(400).send(error.details[0].message);


    const genre=await Genre.findById(req.body.genreId);
    if(!genre) return res.status(400).send('invalid genre..');


const movie=await Movie.findByIdAndUpdate(req.params.movieId,{
title:req.body.title,
genre:{
    _id:genre._id,
    name:genre.name
},
numberInStock:req.body.numberInStock,
dailyRentalRate:req.body.dailyRentalRate
},{new:true})

/*const result=*/await movie.save();// best implementation remove result and set move const not let becouse we dont want to return id to the client
res.send(movie);
console.log(result)


})

module.exports = router;
