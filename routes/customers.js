const {Customer,validate}=require('../models/customer');
const mongoose=require('mongoose');
const express=require('express');
const router=express.Router();

router.get('/',async (req,res)=>{
    const customers=await Customer.find().sort('name');
    res.send(customers);
});

router.post('/',async (req,res)=>{
const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message)



    let customer=new Customer({
        name:req.body.name,
        isGold:req.body.isGold,
        phone:req.body.phone
    });
    customer=await customer.save();
    res.send(customer);

})


router.put('/:customerId',async (req,res)=>{
const customer=await Customer.findById(req.params.customerId);
if(!customer) return res.status(404).send('customer with given id not found');

customer.name=req.body.name;
customer.isGold=req.body.isGold;
customer.phone=req.body.phone;

customer.save();
res.send(customer)
console.log(customer);




});

router.get('/:customerId',async (req,res)=>{
const customer=await Customer.findById(req.params.customerId);
if(!customer) return res.status(404).send('the customer with the given id not found ');
res.send(customer)


})


router.delete('/:customerId',async (req,res)=>{

    

const customer=await Customer.findByIdAndRemove(req.params.customerId);
if(!customer) return res.status(404).send('customer with given id not found');
res.send(customer);


})




module.exports = router;