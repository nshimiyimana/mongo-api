
/*

we don't need thes becouse we have been loaded in user model
const config=require('config');
const Jwt=require('jsonwebtoken');
*/
const bcrypt=require('bcrypt');
const _=require('lodash');
const Joi=require('joi');
const {User}=require('../models/user');
const mongoose=require('mongoose');
const express=require('express');
const { route } = require('./customers');
const router=express.Router();




router.post('/',async (req,res)=>{
    const {error}=validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);

//make sure user not exist in db
let user=await User.findOne({email:req.body.email});
if(!user) return res.status(400).send('invalid email and password. not found');

const validPassword=await bcrypt.compare(req.body.password,user.password); //if true login else incorrect email and password

if(!validPassword) return res.status(400).send('invalid email and password.');

const token=user.generateAuthToken();


res.send(token);


});


function validate(req){
    const schema={
        email:Joi.string().min(5).max(255).required().email(),//email() to valid email type
        password:Joi.string().min(5).max(255).required()
    };

    return Joi.validate(req,schema);
}

module.exports =router;
//api/login


