const Joi=require('joi');

const mongoose=require('mongoose');
const moment=require('moment');

const rentalSchema=new mongoose.Schema({

    customer:{
        type:new mongoose.Schema({
            name:{
                type:String,//to do this is becouse thise are primarily customer properties we want to list in rental collection even 50 cust prop it list only these
                required:true,
                minlength:5,
                maxlength:50
            },
            isGold:{
                type:Boolean,
                default:false                     
            },
            phone:{
                type:String,
                required:true,
                minlength:5,
                maxlength:50
            }
        
        }),
        required:true,
    },
                                                                             
    
    
    
        movie:{
    
    type:new mongoose.Schema({ //reason  same as on customer(the same token as cust) we only need this properties defined here
        title:{
            type:String,
            required:true,
            trim:true,
            minlength:5,
            maxlength:255
        },
    
    
        dailyRentalRate:{
            type:Number,
            required:true,
            min:1,
            max:255
        }
    
    }),
    require:true
        },
    
    
    
        dateOut:{
            type:Date,
            required:true,
            default:Date.now
    
        },
    
    
    
        dateReturned:{
            type:Date
        },
        rentalFee:{
            type:Number,
            min:0
        }
    
    });

//static method
    rentalSchema.statics.lookup=function(customerId,movieId){ // here becouse we use this we don't have to use arrrow function





        return this.findOne( //instead of using Rental.findOne we use this means class
            {
              'customer._id':customerId, //we remove req..customerId&mov becouse we're in model not route if req they tell u req is not defined
              'movie._id' :movieId 
            }
        );





    }//now we have static method


    //instance method
rentalSchema.methods.return= function(){

    this.dateReturned=new Date();
   
       const rentalDays=moment().diff(this.dateOut,'days');//here is to take difference of now and date out of movie
       this.rentalFee=rentalDays*this.movie.dailyRentalRate;//7*2in db=14 here is *no of dollors to pay for movie
       
   
   }
   

const Rental=mongoose.model('Rentals',rentalSchema);




function validateRental(rental) {
    const schema={
        customerId:Joi.objectId().required(),
        movieId:Joi.objectId().required()
    };
    return Joi.validate(rental,schema);
    
}
exports.Rental=Rental;
exports.validate=validateRental;