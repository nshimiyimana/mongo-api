const Jwt=require('jsonwebtoken');
const config=require('config');
const Joi=require('joi');
const mongoose=require('mongoose');

const userSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true,
        minlength:5,
        maxlength:50
    },
    email:{
        type:String,
        required:true,
        minlength:5,
        maxlength:255,
        unique:true
    },
    password:{
        type:String,
        required:true,
        minlength:5,
        maxlength:1024, //higher chars becouse we will need to hash password
       // unique:true we dont need unique
    },
    isAdmin:{
      type:Boolean //this is the same shape as roles and eperations properties below
    },
    roles:[], //roles ex for complex project will have some roles admin and so on here you can pass an array or complex object depends on your app scopen then instead of checking is admin you can check role(in middleware file)
    operations:[] //operation allowed to be done ex to create,delete and so on

});
//on role based authorizatio in payload we add isadmin property allows to make check in db if user is admin

userSchema.methods.generateAuthToken=function(){
    const token=Jwt.sign({_id:this._id,isAdmin:this.isAdmin},config.get('jwtoken'));

return token;
}

const User=mongoose.model('User',userSchema);

function validateUser(user){
    const schema={
        name:Joi.string().min(5).max(50).required(),
        email:Joi.string().min(5).max(255).required().email(),//email() to valid email type
        password:Joi.string().min(5).max(255).required()
    };

    return Joi.validate(user,schema);
}


module.exports.User=User;
module.exports.validate=validateUser;


