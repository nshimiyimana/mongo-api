const helmet=require('helmet');
const compression=require('compression');


module.exports=function(app){
    app.use(helmet());
    app.use(compression());
}//this is module to work in production must be loaded in index.js