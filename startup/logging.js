//when you gonna neeed to use integration test you need to temporarily commentout all about winston-mongodb module becouse it freezes(weak) your terminal window

const winston=require('winston');//this module prevent us from running integration test


require('express-async-errors'); //lo log async errors
//require('winston-mongodb')

module.exports=function(){
    winston.handleExceptions(
        new winston.transports.Console({colorize:true,prettyPrint:true}),
       // new winston.transports.File({filename:'uncatchException.log'})
    );
    
    
    
    
    process.on('unhandledRejection',(ex)=>{
        //console.log('we got UNHANDLED REJECTION')
        throw ex; // if throw ex handleExceptions() method can handle unhundled promise
    
        /*
        winston.error(ex.message,ex)//ex is to store metadata object (stacktrace)
        process.exit(1);
        
        */
    })
    
    
    winston.add(winston.transports.File,{filename:'logfile.log'});
   // winston.add(winston.transports.MongoDB,{db:'mongodb://localhost/vidly'})//and this commented out becouse about winston-mongodb module at this version not work  with integration test and becouse prevent from running int test     //this log work when db is available no trouble at the start of app
    
    
}