
const Jwt=require('jsonwebtoken');
const config=require('config');
module.exports=function (req,res,next){

    //to check if header has token or invalid token

    const token=req.header('x-auth-token');
    
    if(!token) return res.status(401).send('access denied. No token  provided now');//401 unouthorised
    try{
    const decoded=Jwt.verify(token,config.get('jwtoken'));
    req.user=decoded;
    next();
    }
    catch(ex){
        res.status(400).send('invalid token');//wit this error msg we can troubleshoot the authentication issues if i am client i can not access the given endpoint api
    }

}

//module.exports=auth; we get rid of this becouse have been used short cut of directly export function
