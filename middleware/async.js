
//to remove try catch block in each route handler here we put it in single place

module.exports=function asyncMiddleware(handler){  //after this we no longer need trycstch in every route handler and next par
 return async (req,res,next)=>{ //not need async again at the top function asyncMid..


  try{
      await handler(req,res)
          }
      
          catch(ex){
      
      next(ex)
      
          }


 };
 
}