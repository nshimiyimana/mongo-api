module.exports = (validator)=>{ //hre allow us to call validate in route and pass arg of validation function reference

        return (req,res,next)=>{ //when you use this you are able to put this in separate file then use this validate middle ware to every file you want becouse can be dynamically change reference or argument passed in calling validate() in route handler
        const {error}=validator(req.body);
        if(error) return res.status(400).send(error.details[0].message)
        next();  

        
        }
        
      
        
        
        }
