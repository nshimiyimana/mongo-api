




const  mongoose  = require('mongoose');
//all api(http) endpoints tested here in this file

const request=require('supertest'); //this module return function called request with this we can send a request with endpoint
const {Genre}=require('../../models/genre'); //to populate with obj in db requested is that
const { Movie } = require('../../models/movies');
const { Rental } = require('../../models/rental');
const {User}=require('../../models/user');
let server                          //=require('../../index'); to get/load that server we have exported in index.js  to load server not done in this way in integration test

describe('/api/genres',()=>{

beforeEach(()=>{server=require('../../index'); });
afterEach( async ()=>{
    
//always to make sure server close properly all we use await it returns a promise

await server.close();
 await Genre.deleteMany({});//this is good than .remove()
 


});


describe('GET /',()=>{
    it('it should return all genres',async ()=>{
await Genre.collection.insertMany([
    {name:"HAKUZIMANA Aloys"},
    {name:"NZUBAHIMANA Germie"}//here follow schema validation
]);

const res=await request(server).get('/api/genres');                      //in request we pass our server obj  with here you can use .get,.post,.delete,.put..
expect(res.status).toBe(200);
//expect(res.body.length).toEqual(2); //if this condition we don't need await Genre.remove becouse even no of doc grouth no meet with condition no problem
expect(res.body.some((v)=>v.name==='HAKUZIMANA Aloys')).toBeTruthy();
expect(res.body.some((v)=>v.name==='NZUBAHIMANA Germie')).toBeTruthy();





    });
})

describe('GET /:id',()=>{
it('it should return genre if valid id is passed',async ()=>{
const genre=new Genre({
    name:"nshimiyimana jeanluc",
    name:"ndayishimiye jean paul"
});

await genre.save();

const res= await request(server).get('/api/genres/'+genre._id);  //to call endpoint

expect(res.status).toBe(200);




//to make sure that we have this genre in the body of response
/*expect(res.body).toMatchObject(genre); //not run becouse we expect _id to be an object id,but we receive string, so when we mongoose store  id it change into objectId but when we read it we receive string
//return as this    "_id": ObjectID {
    -       "_bsontype": "ObjectID",
    -       "id": Buffer [
    -         95,
    -         101,
    -         206,
    -         102,
    -         28,
    -         134,
    -         160,
    -         26,
    -         151,
    -         17,
    -         94,
    -         62,
    -       ],
    -     },
    +   "_id": "5f65ce661c86a01a97115e3e",
        "name": "ndayishimiye jean paul",

*/

//so we rewrite this last expectation ti something like below
expect(res.body).toHaveProperty('name',genre.name);



});


it('it should return 404 if invalid id is passed',async()=>{
   
    const res= await request(server).get('/api/genres/1');  //to call endpoint with id in parameter
    
    expect(res.status).toBe(404);
    
    
    
    
    
    
    
    });


    it('it should return 404 if no genre with the given id exists',async()=>{
   const id=mongoose.Types.ObjectId();
        const res= await request(server).get('/api/genres/'+id);  //to call endpoint with id in parameter
        
        expect(res.status).toBe(404);
        
        
        
        
        
        
        
        });
    


});


describe('POST /',()=>{


    let token; //inorder to make it empty on test which is not need to use token
let name; //inorder to segregate this name where not needed to make it empty

//writting clean test to refactor to not repeate some li es of code
//Define a happy path, and then in each test,we change
//one parameter that clearly aligns with the name of the test.


const exec=async ()=>{
    //const token=new User().generateAuthToken(); //but there is the problem of to define token in this hoisting function we can not need in all test
    
    return await request(server)
    .post('/api/genres')
    .set('x-auth-token',token)
    .send({name});
}

beforeEach(()=>{
token=new User().generateAuthToken();
name='genre1';

});




    it('should return 401 if client is not logged in',async ()=>{


  
    
   token=''; 
   name; //so in this test we din't have a token,but in other test we do have a token
const res=await exec();

//console.log(token);
expect(res.status).toBe(401);
    //if we display token we see is undefined but if we initialise we get token with epmty even we call exec()
    });
//test invalid inputs

    it('should return 400 if genret is less than 5 characters',async ()=>{
        /*
//before we make request we need to generate authtoken
        const res=await request(server)
          .post('/api/genres')
          //here we need to set header
          .set('x-auth-token',token) //x-auth-token this is key that our authorization middleware looks for,and we pass token
         
         //if we change name:Joi.min(5) test can pass

         
          .send({name:'nday'}) //to set name is less than 5 to not meet with joi validation its send error stack in terminal and this test will failed it require to change min of name in joi
*/



name=1234;


const res=await exec();


          expect(res.status).toBe(400);
          });


          it('should return 400 if genret is more than 50 characters',async ()=>{
            
   //const token=new User().generateAuthToken(); we define in beforeEach() //we don't need token becouse we initialise the tokening before each function
            
name=new Array(54).join('a'); 
const res=await exec();
 expect(res.status).toBe(400);
                      })


//test the happy paths


                      it('should save genre if it is valid',async ()=>{
                        const token=new User().generateAuthToken();
await exec(); //const res we not need it here
const genre=await Genre.find({name:'genre1'});
expect(genre).not.toBeNull();


  });





  it('should return genre if it is valid',async ()=>{
name:'genre1';
    
    const res=await exec();
 expect(res.body).toHaveProperty('_id')//here even not see on value if have id its happy
    expect(res.body).toHaveProperty('name','genre1');
    
    
      });
    });

    

    describe('PUT /:id', () => {
        let token; 
        let newName; 
        let genre; 
        let id; 
    
        const exec = async () => {
          return await request(server)
            .put('/api/genres/' + id)
            .set('x-auth-token', token)
            .send({ name: newName });
        }
    
        beforeEach(async () => {
          // Before each test we need to create a genre and 
          // put it in the database.      
          genre = new Genre({ name: 'genre1' });
          await genre.save();
          
          token = new User().generateAuthToken();     
          id = genre._id; 
          newName = 'updatedName'; 
        })
    
        it('should return 401 if client is not logged in', async () => {
          token = ''; 
    
          const res = await exec();
    
          expect(res.status).toBe(401);
        });
        it('should return 400 if token is invalid',async ()=>{
token='a';

const res=await exec();
expect(res.status).toBe(400);


        });

it('should return 400 if genre is less thna 5 characters',async ()=>{
newName='';
const res=await exec();
expect(res.status).toBe(400);







});
it('shoud should return 400 if genre is greater than 50', async ()=>{

const Myname=new Array(52).join('a');


newName=Myname;


const res=await exec();
expect(res.status).toBe(400);


});


it('should return 404 if id is invalid',async ()=>{

    id = 1;  //check with invalid id becouse mongoose not accept id feel like this one
    const res = await exec();
    expect(res.status).toBe(404);
    
    
    });

it('should return 404 if id is not found',async ()=>{

id=mongoose.Types.ObjectId();//to use valid mpngoose id but not in db
const res=await exec();
expect(res.status).toBe(404);


});

it('should update genre if input is valid',async ()=>{
await exec(); // only we need to call this function that contains put operation we don't need res becouse is to test if updated
const updatedGenre=await Genre.findById(genre._id);
expect(updatedGenre.name).toBe(newName);


});

it('shoud return updated if it is valid',async ()=>{
    //here we need res becouse we need to check if data updated is inside body of response
    const res=await exec();
    //expect(res.body.some((v)=>v.name===newName)).toBeTruthy();

    expect(res.body).toHaveProperty('_id');
    expect(res.body).toHaveProperty('name',newName);
    
    




});



    });




describe( 'DELETE /Id',()=>{
let token;
let genre;
let id;

const exec=async ()=>{
return await request(server)
.delete('/api/genres/' + id)
.set('x-auth-token',token)
.send();

};



beforeEach(async ()=>{
genre =new Genre({
    name:'genre1'
});
await genre.save();
id=genre._id;
token=new User({isAdmin:true}).generateAuthToken();


});


it('should return 401 if client is not logged in',async()=>{
token='';
const res=await exec();
expect(res.status).toBe(401);


});
it('should return 403 if client is not admin',async ()=>{
token=new User({isAdmin:false}).generateAuthToken();
const res=await exec();
expect(res.status).toBe(403);


});


it('should return 404 if id is invalid',async ()=>{//here is to check invalid id with validateObjectid middleware(file)
id=1;
const res=await exec();
expect(res.status).toBe(404);

});

//here is to check if id not found in db but check it becouse is valid id 

it('should return 404 if id not found in db',async ()=>{
//to set valid mongoose  id with genre delete route
 id = mongoose.Types.ObjectId();
const res=await exec();
expect(res.status).toBe(404);


});
it('should  delete genre if dinput is valid',async ()=>{
    await exec();
const genreIndb=await Genre.findById(id);

expect(genreIndb).toBeNull();


})
it('should return removed genre',async ()=>{
const res=await exec();
expect(res.body).toHaveProperty('_id',genre._id.toHexString());
expect(res.body).toHaveProperty('name',genre.name);



})





   });




   
  


      
});






