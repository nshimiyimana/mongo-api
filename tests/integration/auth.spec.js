
const  mongoose  = require('mongoose');
//we are gonna test auth.js(authentication middleware) in separate file
const request=require('supertest');
const { Genre } = require('../../models/genre');
const { User } = require('../../models/user');


let server;


describe('auth middleware',()=>{


//we need a server
beforeEach(()=>{server=require('../../index'); });
afterEach( async()=>{
   await  server.close();
 await Genre.remove({});// deleteMany()this is better better  than .remove() becouse even there is marethan 10 object can delete them to not meet with res.body.length to not exceed 2
});


let token;

//to use mosh technique for writing clean tests
const exec= ()=>{ //here is to define happy path
return request(server)
.post('/api/genres')
.set('x-auth-token',token)
.send({name:'genre1'});

}
beforeEach(()=>{
    token=new User().generateAuthToken();
});


it('should return 401 if no token is provided',async ()=>{
token=''; //if you not set this token empty to expect it require to use status 0f 200 becouse token is ready its true but if expect 200 but no token test will fail set 401 mean you not allowed becouse no token provided
const res=await exec(); //if you set token=null this test will fail becouse it take as invalid token not take it as empty tok(no token provided)
expect(res.status).toBe(401); 

});



it('should return 400 if token is invalid',async ()=>{
  token='a'; //assign simple string to make invalid token
  const res=await exec(); //if you set token=null this test will fail becouse it take as invalid token not take it as empty tok(no token provided)
  expect(res.status).toBe(400); 
  
  });


  it('should return 200 if token is valid',async ()=>{
    //here not reassign token if go on happy path defined above(exec()) becouse we want that our test return staus of 200(true token)
    const res=await exec();
    expect(res.status).toBe(200); 
    
    });



  


});