//in tdd implementations we have test cases
//returns 401 if client is not logged in
//returns 400 if customerId is not provided
//return 400 if movieId is not provided
//return 404 if no rental found for customerId and movieId
//return 400 if rental already processed
//retunr 200 if valid request
//set the return date
//calculate the rantal fee(numberOfDays *  movie.dailyRentalRate)
//increase the stock
//retun the rental







//here is to test api/returns endpoint
const request=require('supertest');
const moment=require('moment');
const {Rental}=require('../../models/rental');
const {User}=require('../../models/user');
const mongoose=require('mongoose'); //to set valid objectId
const { Movie } = require('../../models/movies');


describe('/api/returns',()=>{
//we gonna need load serverand  populate database and before each test to cleanup db after 

let server;
let customerId; // when we post renta; we use this var in body of request
let movieId;
let movie;
let rental; //if we define variable here we can work with it in different functions
let token;


//define happy path allows us sipmly to refactor our test
const exec=()=>{
    return request(server)
    .post('/api/returns')
    .set('x-auth-token',token)
    .send({customerId,movieId})//es6 syntax
   
   
    
};
beforeEach(async ()=>{
    
    server=require('../../index');
   
    //before each test we need to create rental
     customerId=mongoose.Types.ObjectId();
     movieId=mongoose.Types.ObjectId();

     token=new User().generateAuthToken();



//here we have to create new movie to be able to check in stock
//testing movie stock
movie=new Movie(
    {
     _id:movieId,
     title:'12345',
     dailyRentalRate:2,
     genre:{name:'12345'},
     numberInStock:10
    }
);
await movie.save();


    rental= new Rental({
customer:{
_id:customerId,
name:'12345', //becouse must be 5 minimum
phone:'12345', //and here  must be 5 minimum

//we don't need other properties becouse it has default values


},
movie:{
_id:movieId,
title:'12345',
dailyRentalRate:2  //2 dollars


}
    });

    await rental.save();

});
afterEach( async ()=>{
    await server.close();//to close server after each test
 await Rental.deleteMany({})

 await Movie.deleteMany({});
 
 
  
});

/*
this we use it to test if our first case can work run this before start test cases before loading supertest as request() function
//one test to test if it works

it('should works',async ()=>{
const result=await Rental.findById(rental._id);
expect(result).not.toBeNull();
});
*/
//we can excute those implementantion above (first test is okay)






    //let begin to implement test cases aboves

//here as code test first if we run this test we see faled test becouse we don't have this endpoint in prod code
//if we don't have endpoint your'testing express set 404 as received mean endpoint not found

    it('should return 401 if client is not logged in',async ()=>{  
     
  token='';//here we change token param empty
        const res=await exec();
        expect(res.status).toBe(401);
        
        
        });




        it('should return 400 if customerId is not provided',async ()=>{  
            customerId='';

            //another approach we can set customer property in payload when we have body of request is like below
            //delete paylsoad.customerId;
            
              
                const res=await exec();
                  
                   expect(res.status).toBe(400); 
                    
           
            });

            it('should return 400 if movieId is not provided',async ()=>{  
            movieId='';
            const res=await exec();
              
               expect(res.status).toBe(400); 
               
                });


            it('should return 404 if rental not found with customerId/movieId combination',async ()=>{  
              await Rental.remove({});
                const res=await exec();
                  
                   expect(res.status).toBe(404); 
                   
                    });


                    it('should return 400 if is return already processed',async ()=>{  
                       
                        rental.dateReturned=new Date();
                        await rental.save();
                        const res=await exec();
                             expect(res.status).toBe(400); 
                             
                              });


                            it('should return 200 if we have a valid request',async ()=>{

                                const res=await exec();
                                expect(res.status).toBe(200);
                              });
              
                              it('should  set returnDate if input is valid',async ()=>{

                                const res=await exec();

                                const rentalInDb=await Rental.findById(rental._id);
                                const diff=new Date() - rentalInDb.dateReturned;
                                //expect(rentalInDb.dateReturned).toBeDefined(); //in this case to test it we find defference between current date and value of date from databse to make sure dif is lessthan diff instead of using this general methods of toBeDefined()
                            
                            expect(diff).toBeLessThan(10*1000); //this is like 10 secondss
                            });
              
                            it('should  set rentalFee if input is valid',async ()=>{
//rentalfeee is number of days the movie has been out times movie.dailyRentalRate
           //here we require moment library to work with date time either age or now  (moment()=current time)                
rental.dateOut=moment().add(-7,'days').toDate(); //toDate() is to convert this dateOut into standard js date

await rental.save();

const res=await exec();
   const rentalInDb=await Rental.findById(rental._id);
     expect(rentalInDb.rentalFee).toBe(14); // if dailawait rental.save()yRentalRent 2 but if movie its dateOut is 7 days ago(-7 as in moment ) it couse to expect now dailyRentalrent arrive on 14dollars .here we can use gereral method but its good to make more specific                  
                       
                            });




                            it('should  increase the movie stock if input is valid',async ()=>{
                               
                                const res=await exec();
                                   const movieInDb=await Movie.findById(movieId);
                                     expect(movieInDb.numberInStock).toBe(movie.numberInStock +1); //here we can expect with 11 becouse we have 10 in db or do like this in toBe()             
                                                       
                                                            });                       
              
                                                            it('should  return renntal if input is valid',async ()=>{
                               
                                                               const res=await exec();
                                                               /*
                                                               const rentalInDb=await Rental.findById(rental._id)
                                                                     expect(res.body).toHaveProperty('dateOut'); //here we can expect with 11 becouse we have 10 in db or do like this in toBe()             
                                                                     expect(res.body).toHaveProperty('dateReturned');
                                                                     expect(res.body).toHaveProperty('rentalFee');
                                                                     expect(res.body).toHaveProperty('customer'); 
                                                                     expect(res.body).toHaveProperty('movie'); 
                                                                     */
                                                                     expect(Object.keys(res.body)).toEqual(
                                                                    expect.arrayContaining(['dateOut','dateReturned','rentalFee','customer','movie']));
                                                                     
                                                                                            });                       
                                              


});