const {User}=require('../../../models/user');
const jwt=require('jsonwebtoken')
const config=require('config');
const mongoose  = require('mongoose');


describe('generateAuthToken',()=>{

it('should return jwt token',()=>{
    const payload={_id:new mongoose.Types.ObjectId().toHexString(),isAdmin:true};
const user=new User(payload);
const token=user.generateAuthToken();
const decoded=jwt.verify(token,config.get('jwtoken'));
expect(decoded).toMatchObject(payload);//this

});

});/*if not test.json file in config they tell you:'console.error
//WARNING: NODE_ENV value of 'test' did not match any deployment config file names.
//'  */
