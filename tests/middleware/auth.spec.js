
//this is on auth middleware to make sure(verify) if client sends a valid jsonwebtoken request that user be populated with the payload of jsonwebtoken
const auth = require("../../middleware/auth");

const {User}=require('../../models/user');
const mongoose=require('mongoose'); //load monoose to generate objectId
//and we need load middleware module

describe('auth middleware',()=>{
it('should populate req.user with the payload of valid JWT',()=>{

const user={_id:mongoose.Types.ObjectId().toHexString(),// to not be string if decodes it into json*/ 
    isAdmin:true
};

const token=new User(user).generateAuthToken();

//let's we create request object
const req={
    header:jest.fn().mockReturnValue(token)
};
const res={};
const next=jest.fn();
auth(req,res,next);
//expect(req.user).toBeDefined(); //user property is from header as req payload 
//then change from test in gereral put it into specific
expect(req.user).toMatchObject(user);
})

});